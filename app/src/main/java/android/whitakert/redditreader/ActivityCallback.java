package android.whitakert.redditreader;


import android.net.Uri;

public interface ActivityCallback {
    void onPostSelected(Uri reddiPostUri);
}
